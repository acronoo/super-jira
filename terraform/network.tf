# Определяем внешнюю сеть, используемую для Floating IP
data "vkcs_networking_network" "extnet" {
  name = "ext-net"
}

# Определяем внутреннюю сеть с именем "VM-Deploy_network"
data "vkcs_networking_network" "network" {
  name = "VM-Deploy_network"
}

# Определяем подсеть с именем "subnet_3889" и связываем ее с внутренней сетью "VM-Deploy_network"
data "vkcs_networking_subnet" "subnetwork" {
  name       = "subnet_3889"
  network_id = data.vkcs_networking_network.network.id
}

# Создаем группу безопасности (Security Group) с именем "secgroup"
resource "vkcs_networking_secgroup" "secgroup" {
  name        = "secgroup"
  description = "Security group for terraform instances"
}

# Создаем правило для группы безопасности, разрешающее входящий SSH-трафик
resource "vkcs_networking_secgroup_rule" "secgroup_rule_ssh" {
  direction        = "ingress"
  port_range_max   = 22
  port_range_min   = 22
  protocol         = "tcp"
  remote_ip_prefix = "0.0.0.0/0"  # Разрешить SSH из любых IP-адресов
  security_group_id = vkcs_networking_secgroup.secgroup.id
  description      = "Open SSH (22) Port"
}

# Создаем правило для группы безопасности, разрешающее входящий HTTP-трафик
resource "vkcs_networking_secgroup_rule" "secgroup_rule_http" {
  direction        = "ingress"
  port_range_max   = 8080
  port_range_min   = 8080
  protocol         = "tcp"
  remote_ip_prefix = "0.0.0.0/0"  # Разрешить HTTP из любых IP-адресов
  security_group_id = vkcs_networking_secgroup.secgroup.id
  description      = "Open HTTP (8080) Port"
}

# Создаем правило для группы безопасности
resource "vkcs_networking_secgroup_rule" "secgroup_rule_http1" {
  direction        = "ingress"
  port_range_max   = 8081
  port_range_min   = 8081
  protocol         = "tcp"
  remote_ip_prefix = "0.0.0.0/0"
  security_group_id = vkcs_networking_secgroup.secgroup.id
  description      = "Open HTTP (8081) Port"
}

# Создаем правило для группы безопасности
resource "vkcs_networking_secgroup_rule" "secgroup_rule_prom" {
  direction        = "ingress"
  port_range_max   = 9090
  port_range_min   = 9090
  protocol         = "tcp"
  remote_ip_prefix = "0.0.0.0/0"
  security_group_id = vkcs_networking_secgroup.secgroup.id
  description      = "Open HTTP (9090) Port"
}

# Создаем правило для группы безопасности
resource "vkcs_networking_secgroup_rule" "secgroup_rule_graf" {
  direction        = "ingress"
  port_range_max   = 3000
  port_range_min   = 3000
  protocol         = "tcp"
  remote_ip_prefix = "0.0.0.0/0"
  security_group_id = vkcs_networking_secgroup.secgroup.id
  description      = "Open HTTP (3000) Port"
}

# Создаем правило для группы безопасности
resource "vkcs_networking_secgroup_rule" "secgroup_rule_alltcp" {
  direction        = "ingress"
  protocol         = "tcp"
  remote_ip_prefix = "10.0.0.0/24"
  security_group_id = vkcs_networking_secgroup.secgroup.id
  description      = "Open all TCP inside"
}

# Создаем правило для группы безопасности
resource "vkcs_networking_secgroup_rule" "secgroup_rule_alludp" {
  direction        = "ingress"
  protocol         = "udp"
  remote_ip_prefix = "10.0.0.0/24"
  security_group_id = vkcs_networking_secgroup.secgroup.id
  description      = "Open all UDP inside"
}

# Создаем ресурс для выделения внешнего IP-адреса (Floating IP) и его привязки к порту
resource "vkcs_networking_floatingip" "lb_fip" {
  pool    = "ext-net"  # Выбираем пул для Floating IP из внешней сети
  port_id = vkcs_lb_loadbalancer.lb.vip_port_id  # Привязываем IP к порту балансировщика
}

# Создаем внешний IP-адрес для mon
resource "vkcs_networking_floatingip" "mon_fip" {
  pool    = "ext-net"  # Выберите пул для Floating IP из внешней сети
}

# Привязываем внешний IP-адрес
resource "vkcs_compute_floatingip_associate" "mon_fip" {
  floating_ip = vkcs_networking_floatingip.mon_fip.address
  instance_id = vkcs_compute_instance.basic[0].id
}

output "mon_instance_ip" {
  value = vkcs_networking_floatingip.mon_fip.address
}

# Роутер уже присутствует в текущей инфраструктуре
# Создаем роутер для коннекта сетей     
#resource "vkcs_networking_router" "router" {
 # name                = "router"
 # admin_state_up      = true
 # external_network_id = data.vkcs_networking_network.extnet.id
#}

# Соединяем роутер с сетями
#resource "vkcs_networking_router_interface" "router_interface" {
#  router_id = vkcs_networking_router.router.id
#  subnet_id = vkcs_networking_subnet.subnetwork.id
#}
