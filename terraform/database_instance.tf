# Определение конфигураций виртуальных машин для базы данных (flavor)
data "vkcs_compute_flavor" "db" {
  vcpus = 2
  ram   = 8192
}

# Создание кластера базы данных
resource "vkcs_db_cluster" "db-cluster" {
  name        = "db-cluster"
  datastore {
    type    = "postgresql"
    version = "14"
  }
  flavor_id       = data.vkcs_compute_flavor.db.id
  keypair         = var.key_pair_name
  availability_zone = var.availability_zone_name
  volume_size = 10
  volume_type = "ceph-ssd"
  disk_autoexpand {
    autoexpand    = true
    max_disk_size = 1000
  }
  cluster_size = 3

  network {
    uuid      = data.vkcs_networking_network.network.id
  }

  capabilities {
    name = "node_exporter"
    settings = {
      "listen_port" : "9100"
    }
  }

  capabilities {
    name = "postgres_exporter"
    settings = {
      "listen_port" : "9187"
    }
  }

  capabilities {
    name = "postgres_extensions"
  }

  # Настроим расписание регулярных бэкапов
  backup_schedule {
    name = "24hours_backup"
    start_hours = 18
    start_minutes = 20
    interval_hours = 24
    keep_count = 1
  }
}

# Создаем базу данных
resource "vkcs_db_database" "jiradb" {
  name    = "jiradb"
  dbms_id = vkcs_db_cluster.db-cluster.id
  charset = "utf8"
  #collate = "en_US.UTF-8"
}

# Создаем пользователя БД
resource "vkcs_db_user" "db-user" {
  name        = "jira"
  password    = var.db_user_password
  dbms_id     = vkcs_db_cluster.db-cluster.id
  databases   = [vkcs_db_database.jiradb.name]
}

# Получаем информацию о балансировщике нагрузки
data "vkcs_lb_loadbalancer" "loadbalancer" {
  id = "${vkcs_db_cluster.db-cluster.loadbalancer_id}"
}

data "vkcs_networking_port" "loadbalancer-port" {
  port_id = "${data.vkcs_lb_loadbalancer.loadbalancer.vip_port_id}"
}

# Выводим IP-адреса кластера в выходных данных
output "cluster_ips" {
  value = "${data.vkcs_networking_port.loadbalancer-port.all_fixed_ips}"
  description = "IP addresses of the cluster."
}
