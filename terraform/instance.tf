# Определяем конфигурации виртуальных машин (flavors)
data "vkcs_compute_flavor" "basic" {
  vcpus = 2
  ram   = 2048
}

data "vkcs_compute_flavor" "jira" {
  vcpus = 4
  ram   = 4096
}

# Определяем образ для виртуальных машин на основе переменной var.image_flavor
data "vkcs_images_image" "compute" {
  name = var.image_flavor
}

# Создаем виртуальные машины для Jira с использованием данных о конфигурации
resource "vkcs_compute_instance" "jira" {
  count                   = var.vm_count_jira
  name                    = var.vm_names_jira[count.index]
  flavor_id               = data.vkcs_compute_flavor.jira.id
  key_pair                = var.key_pair_name
  security_groups         = ["default", "secgroup"]
  availability_zone       = var.availability_zone_name

  block_device {
    uuid                  = data.vkcs_images_image.compute.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = "ceph-hdd"
    volume_size           = 20
    boot_index            = 0
    delete_on_termination = true
  }

  network {
    uuid = data.vkcs_networking_network.network.id
    fixed_ip_v4 = "10.0.0.${count.index + 10}"
  }
}

/*# Создаем null_resource для монтирования NFS шары
resource "null_resource" "mount_nfs" {
  count = var.vm_count_jira

  triggers = {
    instance_id = vkcs_compute_instance.jira[count.index].id
  }

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      host        = element(vkcs_compute_instance.jira[*].network[0].fixed_ip_v4, count.index)
      user        = "ubuntu"
      private_key = file("/home/gitlab-runner/.ssh/GB-VM-Deploy-A4qSYnzi.pem")
      agent       = false
      ssh_command = "ssh -o StrictHostKeyChecking=no -i /home/gitlab-runner/.ssh/GB-VM-Deploy-A4qSYnzi.pem"
    }

    inline = [
      "sudo apt-get update",
      "sudo apt-get install -y nfs-common",
      "sudo mkdir -p /var/jira2",
      "sudo mount -t nfs ${vkcs_sharedfilesystem_share.share.export_location_path} /var/jira2",
      "echo '${vkcs_sharedfilesystem_share.share.export_location_path} /var/jira2 nfs rw,sync,hard,intr 0 0' | sudo tee -a /etc/fstab",
    ]
  }
}*/

# Создаем остальные виртуальные машины
resource "vkcs_compute_instance" "basic" {
  count                   = var.vm_count
  name                    = var.vm_names[count.index]
  flavor_id               = data.vkcs_compute_flavor.basic.id
  key_pair                = var.key_pair_name
  security_groups         = ["default", "secgroup"]
  availability_zone       = var.availability_zone_name

  block_device {
    uuid                  = data.vkcs_images_image.compute.id
    source_type           = "image"
    destination_type      = "volume"
    volume_type           = "ceph-hdd"
    volume_size           = 20
    boot_index            = 0
    delete_on_termination = true
  }

  network {
    uuid = data.vkcs_networking_network.network.id
    fixed_ip_v4 = "10.0.0.${count.index + 20}"
  }
}

# Создаем балансировщик с указанием подсети, в которой он будет работать
resource "vkcs_lb_loadbalancer" "lb" {
  name = "lb"
  vip_subnet_id = data.vkcs_networking_subnet.subnetwork.id
}

# Создаем Listener HTTP на балансировщике
resource "vkcs_lb_listener" "http_listener" {
  name           = "http_listener"
  protocol       = "HTTP"
  protocol_port  = 8080
  loadbalancer_id = vkcs_lb_loadbalancer.lb.id
}

# Создаем пул для балансировки HTTP-трафика
resource "vkcs_lb_pool" "http_pool" {
  name     = "http-pool"
  protocol = "HTTP"
  lb_method   = "ROUND_ROBIN"
  listener_id = vkcs_lb_listener.http_listener.id
}

# Создаем монитор для проверки доступности серверов в пуле
resource "vkcs_lb_monitor" "http_monitor" {
  name           = "http_monitor"
  delay          = 5
  max_retries    = 3
  timeout        = 5
  pool_id        = vkcs_lb_pool.http_pool.id
}

# Создаем членов пула для Jira инстансов
resource "vkcs_lb_member" "member_http" {
  for_each = { for idx, instance in vkcs_compute_instance.jira : idx => instance }
  address       = each.value.network[0].fixed_ip_v4
  protocol_port = 8080
  pool_id       = vkcs_lb_pool.http_pool.id
  subnet_id     = data.vkcs_networking_subnet.subnetwork.id
  weight        = 5
}

# Выводим IP-адреса экземпляров в выходных данных
output "instance_ips" {
  value = concat(
    [vkcs_networking_floatingip.lb_fip.address],  # Внешний IP-адрес load balancer
    vkcs_compute_instance.jira[*].network[0].fixed_ip_v4,  # Внутренние IP-адреса jira инстансов
    vkcs_compute_instance.basic[*].network[0].fixed_ip_v4  # Внутренние IP-адреса basic инстансов
  )
}
